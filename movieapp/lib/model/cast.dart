class Cast {
  final int castId;
  final String character;
  final String name;
  final String img;
  final int id;

  Cast(this.castId,
        this.character,
         this.name,
         this.img,
         this.id);

  Cast.fromJson(Map<String, dynamic> json)
      : castId = json["cast_id"],
        character = json["character"],
        name = json["name"],
        img = json["profile_path"],
        id = json["id"];
}
