import 'package:movieapp/model/person_detail.dart';

class PersonDetailResponse {
  final PersonDetail personDetail;
  final String error;

  PersonDetailResponse(this.personDetail, this.error);

  PersonDetailResponse.fromJson(Map<String, dynamic> json)
      : personDetail = PersonDetail.fromJson(json),
        error = "";

  PersonDetailResponse.withError(String errorValue)
      : personDetail =
            PersonDetail(null, '', null, null, '', null, null, '', ''),
        error = errorValue;
}
