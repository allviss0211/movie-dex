import 'package:movieapp/model/movie_response.dart';
import 'package:movieapp/model/person_detail_response.dart';
import 'package:movieapp/model/person_movie_credit.dart';

import 'movie.dart';

class PersonCreditMovieResponse {
  final List<Movie> creditMovies;
  final String error;

  PersonCreditMovieResponse(this.creditMovies, this.error);

  PersonCreditMovieResponse.fromJson(Map<String, dynamic> json)
      :creditMovies =
        (json["cast"] as List).map((i) => new Movie.fromJson(i)).toList(),
        error = "";

  PersonCreditMovieResponse.withError(String errorValue)
      :       creditMovies = List(),
        error = errorValue;
}
