import 'dart:ui';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:movieapp/bloc/get_movie_videos_bloc.dart';
import 'package:movieapp/model/movie.dart';
import 'package:movieapp/model/video.dart';
import 'package:movieapp/model/video_response.dart';
import 'package:movieapp/screens/detail_screen.dart';
import 'package:movieapp/style/theme.dart' as Style;
import 'package:movieapp/widgets/casts.dart';
import 'package:movieapp/widgets/movie_info.dart';
import 'package:movieapp/widgets/similar_movies.dart';
import 'package:sliver_fab/sliver_fab.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

import 'video_player.dart';

class PageMovieDetailScreen extends StatefulWidget {
  final int index;
  final List<Movie> listMovies;
  PageMovieDetailScreen({Key key, @required this.listMovies, this.index})
      : super(key: key);
  @override
  _MovieDetailScreenState createState() =>
      _MovieDetailScreenState(listMovies, index);
}

class _MovieDetailScreenState extends State<PageMovieDetailScreen> {
  final List<Movie> listMovies;
  int index;
  final _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
  _MovieDetailScreenState(this.listMovies, this.index);

  final movieVideosBloc = MovieVideosBloc();

  PageController pageController;

  @override
  void initState() {
    super.initState();
    pageController = PageController(initialPage: this.index, keepPage: true);
    movieVideosBloc..getMovieVideos(listMovies[index].id);
  }

//   @override
//   void dispose() {
//     super.dispose();
//     movieVideosBloc..drainStream();
//   }

  Future<void> _refresh() async {
    movieVideosBloc..getMovieVideos(listMovies[index].id);
  }

  @override
  Widget build(BuildContext context) {
    print("build detail");
    return Scaffold(
      backgroundColor: Style.Colors.mainColor,
      body: RefreshIndicator(
        key: _refreshIndicatorKey,
        onRefresh: _refresh,
        child: PageView.builder(
          scrollDirection: Axis.horizontal,
          controller: pageController,
          onPageChanged: (value) {
            setState(() {
              this.index = value;
            });
          },
          itemCount: this.listMovies.length,
          itemBuilder: (context, index) {
            return MovieDetailScreen(
              movie: this.listMovies[index],
            );
          },
        ),
      ),
    );
  }

  Widget _buildLoadingWidget() {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [],
    ));
  }

  Widget _buildErrorWidget(String error) {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text("Error occured: $error"),
      ],
    ));
  }

  Widget _buildVideoWidget(VideoResponse data) {
    List<Video> videos = data.videos;
    return FloatingActionButton(
      backgroundColor: Style.Colors.secondColor,
      onPressed: () {
        if (videos.length != 0) {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => VideoPlayerScreen(
                controller: YoutubePlayerController(
                  initialVideoId: videos[0].key.toString(),
                  flags: YoutubePlayerFlags(
                    autoPlay: true,
                    mute: false,
                  ),
                ),
              ),
            ),
          );
        } else {
          showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                backgroundColor: Style.Colors.mainColor,
                shape: RoundedRectangleBorder(
                    side: BorderSide(
                      color: Style.Colors.secondColor,
                      width: 1,
                    ),
                    borderRadius: BorderRadius.circular(20)),
                title: Text(
                  "Notice",
                  style: TextStyle(color: Style.Colors.secondColor),
                ),
                content: Text(
                  "This film has no Trailer video.",
                  style: TextStyle(color: Colors.white),
                ),
              );
            },
          );
        }
      },
      child: Icon(Icons.play_arrow),
    );
  }
}
