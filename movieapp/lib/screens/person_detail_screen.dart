import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:movieapp/bloc/get_person_detail_bloc.dart';
import 'package:movieapp/bloc/search_movie.dart';
import 'package:movieapp/model/person_detail.dart';
import 'package:movieapp/model/person_detail_response.dart';
import 'package:movieapp/style/expanded_text.dart';
import 'package:movieapp/widgets/known_for_film.dart';
import 'package:movieapp/widgets/similar_movies.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:movieapp/style/theme.dart' as Style;

class DetailPersonScreen extends StatefulWidget {
  final int idPerson;
  DetailPersonScreen({this.idPerson});

  @override
  _DetailPersonScreenState createState() =>
      _DetailPersonScreenState();
}

class _DetailPersonScreenState extends State<DetailPersonScreen> {
  final PanelController _panelController = PanelController();

  final personDetailBloc = PersonDetailBloc();

  @override
  void initState() {
    super.initState();
    print("idPerson: ${widget.idPerson}");
    personDetailBloc..getPersonDetail(widget.idPerson);
  }

  // @override
  // void dispose() {
  //   super.dispose();
  //   personDetailBloc..drainStream();
  // }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<PersonDetailResponse>(
      stream: personDetailBloc.subject.stream,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          if (snapshot.data.error != null && snapshot.data.error.length > 0) {
            return _buildErrorWidget(snapshot.data.error);
          }
          return _buildPersonDetailWidget(snapshot.data);
        } else if (snapshot.hasError) {
          return _buildErrorWidget(snapshot.error);
        } else {
          return _buildLoadingWidget();
        }
      },
    );
  }

  Widget _buildLoadingWidget() {
    return Center(
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      Container(
        decoration: BoxDecoration(
            color: Style.Colors.mainColor, shape: BoxShape.circle),
        height: 50.0,
        width: 50.0,
        child: CircularProgressIndicator(
          valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
          strokeWidth: 4.0,
        ),
      )
    ]));
  }

  Widget _buildErrorWidget(String error) {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text("Error occured: $error"),
      ],
    ));
  }

  Widget _buildPersonDetailWidget(PersonDetailResponse data) {
    final PersonDetail personDetail = data.personDetail;
    return Stack(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: CachedNetworkImage(
            fit: BoxFit.cover,
            imageUrl: "https://image.tmdb.org/t/p/original/" +
                personDetail.profileImg.toString(),
            errorWidget: (context, url, error) {
              return Image.asset(
                "images/Unknow_profile.png",
                fit: BoxFit.cover,
              );
            },
          ),
        ),

//        Scaffold(
//          backgroundColor: Colors.transparent,
//            appBar: AppBar(
//              backgroundColor: Colors.transparent,
//              elevation: 0,
//              actions: <Widget>[
//                Container(
//                      child: IconButton(
//                        icon: Icon(FontAwesomeIcons.arrowAltCircleUp),
//                        iconSize: 30,
//                        color: Colors.white,
//                        onPressed: () => _panelController.open(),
//                      ),
//                    ),
//                Container(
//                      child: IconButton(
//                        icon: Icon(FontAwesomeIcons.arrowAltCircleDown),
//                        iconSize: 30,
//                        color: Colors.white,
//                        onPressed: () => _panelController.close(),
//                      ),
//                    ),
//              ],
//            ),
//            body: SlidingUpPanel(
//            controller: _panelController,
//            collapsed: Container(
//              decoration: BoxDecoration(
//                border: Border(
//                    top: BorderSide(color: Style.Colors.secondColor, width: 7)),
//                color: Style.Colors.mainColor,
//              ),
//              child: Center(
//                child: Text(
//                  "${personDetail.name}",
//                  textAlign: TextAlign.center,
//                  style: TextStyle(
//                      color: Colors.white,
//                      fontSize: 30,
//                      fontWeight: FontWeight.bold),
//                ),
//              ),
//            ),
//            minHeight: 100,
//            maxHeight: MediaQuery.of(context).size.height * 2/3,
//            borderRadius: BorderRadius.only(
//              topLeft: Radius.circular(20),
//              topRight: Radius.circular(20),
//            ),
//            color: Style.Colors.mainColor.withOpacity(0.8),
//            panel: Padding(
//              padding: const EdgeInsets.symmetric(horizontal: 10),
//              child: SingleChildScrollView(
//                child: Column(
//                  children: <Widget>[
//                    Container(
//                      alignment: Alignment.center,
//                      width: double.infinity,
//                      decoration: BoxDecoration(
//                          border: Border(
//                              bottom:
//                                  BorderSide(color: Style.Colors.secondColor))),
//                      padding: const EdgeInsets.all(10),
//                      child: Text(
//                        "${personDetail.name}",
//                        style: TextStyle(
//                            color: Colors.white,
//                            fontSize: 30,
//                            fontWeight: FontWeight.bold),
//                      ),
//                    ),
//                    Container(
//                      alignment: Alignment.centerLeft,
//                      padding: const EdgeInsets.only(top: 20),
//                      child: Text(
//                        "Biography",
//                        style: TextStyle(
//                            color: Style.Colors.secondColor,
//                            fontSize: 20,
//                            fontWeight: FontWeight.bold),
//                      ),
//                    ),
//                    Container(
//                        padding: const EdgeInsets.only(bottom: 20, top: 20),
//                        child: ExpandableText("${personDetail.biography}")),
//                    Padding(
//                      padding: const EdgeInsets.only(bottom: 20),
//                      child: Row(
//                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                        children: <Widget>[
//                          Column(
//                            mainAxisAlignment: MainAxisAlignment.start,
//                            crossAxisAlignment: CrossAxisAlignment.start,
//                            children: <Widget>[
//                              Text(
//                                "Birthday",
//                                style: TextStyle(
//                                    color: Style.Colors.secondColor,
//                                    fontSize: 20,
//                                    fontWeight: FontWeight.bold),
//                              ),
//                              Text(
//                                "${personDetail.birthday.toString()}",
//                                style: TextStyle(
//                                    color: Colors.white,
//                                    fontSize: 18,
//                                    fontStyle: FontStyle.italic,
//                                    fontWeight: FontWeight.w300),
//                              ),
//                            ],
//                          ),
//                          Column(
//                            mainAxisAlignment: MainAxisAlignment.start,
//                            crossAxisAlignment: CrossAxisAlignment.start,
//                            children: <Widget>[
//                              Text(
//                                "Gender",
//                                style: TextStyle(
//                                    color: Style.Colors.secondColor,
//                                    fontSize: 20,
//                                    fontWeight: FontWeight.bold),
//                              ),
//                              Text(
//                                personDetail.gender == 1 ? "Female" : "Male",
//                                style: TextStyle(
//                                    color: Colors.white,
//                                    fontSize: 18,
//                                    fontStyle: FontStyle.italic,
//                                    fontWeight: FontWeight.w300),
//                              ),
//                            ],
//                          ),
//                          Column(
//                            mainAxisAlignment: MainAxisAlignment.start,
//                            crossAxisAlignment: CrossAxisAlignment.start,
//                            children: <Widget>[
//                              Text(
//                                "Known For",
//                                style: TextStyle(
//                                    color: Style.Colors.secondColor,
//                                    fontSize: 20,
//                                    fontWeight: FontWeight.bold),
//                              ),
//                              Text(
//                                "${personDetail.known.toString()}",
//                                style: TextStyle(
//                                    color: Colors.white,
//                                    fontSize: 18,
//                                    fontStyle: FontStyle.italic,
//                                    fontWeight: FontWeight.w300),
//                              ),
//                            ],
//                          )
//                        ],
//                      ),
//                    ),
//                    Padding(
//                      padding: const EdgeInsets.only(bottom: 0),
//                      child: Row(
//                        //mainAxisAlignment: MainAxisAlignment.spaceAround,
//                        children: <Widget>[
//                          Column(
//                            mainAxisAlignment: MainAxisAlignment.start,
//                            crossAxisAlignment: CrossAxisAlignment.start,
//                            children: <Widget>[
//                              Text(
//                                "Place of birth",
//                                style: TextStyle(
//                                    color: Style.Colors.secondColor,
//                                    fontSize: 20,
//                                    fontWeight: FontWeight.bold),
//                              ),
//                              Text(
//                                "${personDetail.place.toString()}",
//                                style: TextStyle(
//                                    color: Colors.white,
//                                    fontSize: 18,
//                                    fontStyle: FontStyle.italic,
//                                    fontWeight: FontWeight.w300),
//                              ),
//                            ],
//                          )
//                        ],
//                      ),
//                    ),
//                    KnowForFilm(id: personDetail.id,)
//                  ],
//                ),
//              ),
//            ),
//          ),
//        ),

        Scaffold(
            backgroundColor: Colors.transparent,
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
            ),
            body: DraggableScrollableSheet(
              initialChildSize: 0.16,
              maxChildSize: 0.8,
              minChildSize: 0.16,
              builder: (context, scrollController) {
                return SingleChildScrollView(
                  controller: scrollController,
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20),
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border(
                            top: BorderSide(
                                color: Style.Colors.secondColor, width: 15)),
                        color: Style.Colors.mainColor.withOpacity(0.8),
                      ),
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Column(
                        children: <Widget>[
                          Container(
                            alignment: Alignment.center,
                            width: double.infinity,
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(
                                        color: Style.Colors.secondColor))),
                            padding: const EdgeInsets.fromLTRB(10, 20, 10, 20),
                            child: Text(
                              "${personDetail.name}",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 30,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            padding: const EdgeInsets.only(top: 20),
                            child: Text(
                              "Biography",
                              style: TextStyle(
                                  color: Style.Colors.secondColor,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                              padding:
                                  const EdgeInsets.only(bottom: 20, top: 10),
                              child:
                                  ExpandableText("${personDetail.biography}")),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "Birthday",
                                      style: TextStyle(
                                          color: Style.Colors.secondColor,
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      "${personDetail.birthday.toString()}",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 18,
                                          fontStyle: FontStyle.italic,
                                          fontWeight: FontWeight.w300),
                                    ),
                                  ],
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "Gender",
                                      style: TextStyle(
                                          color: Style.Colors.secondColor,
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      personDetail.gender == 1
                                          ? "Female"
                                          : "Male",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 18,
                                          fontStyle: FontStyle.italic,
                                          fontWeight: FontWeight.w300),
                                    ),
                                  ],
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "Known For",
                                      style: TextStyle(
                                          color: Style.Colors.secondColor,
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      "${personDetail.known.toString()}",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 18,
                                          fontStyle: FontStyle.italic,
                                          fontWeight: FontWeight.w300),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 20),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Place of birth",
                                  style: TextStyle(
                                      color: Style.Colors.secondColor,
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                ),
                                Row(
                                  children: <Widget>[
                                    Flexible(
                                                                          child: Text(
                                        "${personDetail.place.toString()}",
                                        maxLines: 2,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 18,
                                            fontStyle: FontStyle.italic,
                                            fontWeight: FontWeight.w300),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          KnowForFilm(
                            id: personDetail.id,
                          )
                        ],
                      ),
                    ),
                  ),
                );
              },
            )),
      ],
    );
  }
}
