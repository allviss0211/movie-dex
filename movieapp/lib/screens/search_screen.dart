import 'package:cached_network_image/cached_network_image.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:movieapp/bloc/search_movie.dart';
import 'package:movieapp/model/movie.dart';
import 'package:movieapp/model/movie_detail.dart';
import 'package:movieapp/model/movie_response.dart';
import 'package:movieapp/screens/detail_screen.dart';
import 'package:movieapp/style/theme.dart' as Style;

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  TextEditingController searchController = TextEditingController();
  
  final searchMovieBloc = SearchMovieBloc();


  @override
  void dispose() {
    super.dispose();
    searchController.clear();
    searchMovieBloc..drainStream();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Style.Colors.mainColor,
      appBar: AppBar(
        backgroundColor: Style.Colors.mainColor,
        title: TextFormField(
          onFieldSubmitted: (value) {
            if(value.isNotEmpty) {
              searchMovieBloc..searchMovie(value);
            }
          },
          controller: searchController,
          style: TextStyle(color: Colors.white),
          decoration: InputDecoration(
              hintText: "Search...",
              hintStyle: TextStyle(color: Colors.grey.shade300),
              icon: Icon(
                Icons.search,
                color: Colors.white,
              ),
              border: InputBorder.none),
        ),
      ),
      body: StreamBuilder<MovieResponse>(
        stream: searchMovieBloc.subject.stream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.error != null && snapshot.data.error.length > 0) {
              return _buildErrorWidget(snapshot.data.error);
            }
            return _buildHomWidget(snapshot.data);
          } else {
            return _buildErrorWidget(snapshot.error);
          }
        },
      ),
    );
  }

  Widget _buildHomWidget(MovieResponse data) {
    List<Movie> movies = data.movies;
    if (movies.length == 0) {
      return Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Column(
              children: <Widget>[
                Text(
                  "No More Movies",
                  style: TextStyle(color: Colors.white, fontSize: 18),
                )
              ],
            )
          ],
        ),
      );
    } else
      return Container(
        height: MediaQuery.of(context).size.height,
        padding: EdgeInsets.only(left: 10.0),
        child: ListView.builder(
          itemCount: movies.length,
          itemBuilder: (context, index) {
            return Padding(
              padding: EdgeInsets.only(top: 10.0, bottom: 10.0, right: 15.0),
              child: GestureDetector(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => MovieDetailScreen(movie: movies[index],),
              ));
                },
                child: Row(
                  children: <Widget>[
                    Hero(
                      tag: movies[index].id,
                      child: Container(
                        width: MediaQuery.of(context).size.width * 0.3,
                        height: 180.0,
                        decoration: new BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(2.0)),
                          shape: BoxShape.rectangle,
                        ),
                        child: CachedNetworkImage(
                          imageUrl: "https://image.tmdb.org/t/p/w200/" +
                              movies[index].poster.toString(),
                          errorWidget: (context, url, error) =>
                              Image.asset('images/Unknow_profile.png'),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: Container(
                        height: 180,
                        width: MediaQuery.of(context).size.width * 0.6,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              child: Text(
                                movies[index].title,
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    height: 1.4,
                                    color: Colors.white,
                                    fontSize: 20.0),
                              ),
                            ),
                            Container(
                              child: Text(
                                movies[index].originalTitle.toString(),
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    height: 1.4,
                                    color: Colors.white,
                                    fontSize: 16.0),
                              ),
                            ),
                            Row(
                              children: <Widget>[
                                Text(
                                  movies[index].rating.toString(),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  width: 5.0,
                                ),
                                RatingBar(
                                  itemSize: 12.0,
                                  initialRating: movies[index].rating / 2,
                                  minRating: 1,
                                  direction: Axis.horizontal,
                                  allowHalfRating: true,
                                  itemCount: 5,
                                  itemPadding:
                                      EdgeInsets.symmetric(horizontal: 2.0),
                                  itemBuilder: (context, _) => Icon(
                                    EvaIcons.star,
                                    color: Style.Colors.secondColor,
                                  ),
                                  onRatingUpdate: (rating) {
                                    print(rating);
                                  },
                                )
                              ],
                            ),
                            Container(
                              child: Text(
                                movies[index].overview,
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    height: 1.4,
                                    color: Colors.white,
                                    fontSize: 13.0),
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            );
          },
        ),
      );
  }

  Widget _buildErrorWidget(String error) {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Error occured: $error",
          style: TextStyle(color: Style.Colors.mainColor),
        ),
      ],
    ));
  }

  Widget _buildLoadingWidget() {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          height: 25.0,
          width: 25.0,
          child: CircularProgressIndicator(
            valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
            strokeWidth: 4.0,
          ),
        )
      ],
    ));
  }
}
