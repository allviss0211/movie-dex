import 'package:flutter/material.dart';

class ExpandableText extends StatefulWidget {
  ExpandableText(this.text);
  final String text;

  @override
  _ExpandableTextState createState() => new _ExpandableTextState();
}

class _ExpandableTextState extends State<ExpandableText>
    with TickerProviderStateMixin<ExpandableText> {
  bool isExpanded = false;
  @override
  Widget build(BuildContext context) {
    return new Column(children: <Widget>[
      new AnimatedSize(
          vsync: this,
          duration: const Duration(milliseconds: 1000),
          child: ConstrainedBox(
              constraints: isExpanded
                  ? BoxConstraints()
                  : BoxConstraints(maxHeight: 100.0),
              child: Text(
                widget.text,
                softWrap: true,
                overflow: TextOverflow.fade,
                textAlign: TextAlign.justify,
                style: TextStyle(fontSize: 16, color: Colors.white, fontStyle: FontStyle.italic, fontWeight: FontWeight.w300),
              ))),
      isExpanded
          ? FlatButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              child: const Text(
                'Collapsed',
                style: TextStyle(color: Colors.blue),
              ),
              highlightColor: Colors.blue.shade50,
              onPressed: () => setState(() => isExpanded = false))
          : FlatButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              child: const Text(
                'Expanded',
                style: TextStyle(color: Colors.blue),
              ),
              highlightColor: Colors.blue.shade50,
              onPressed: () => setState(() => isExpanded = true))
    ]);
  }
}
